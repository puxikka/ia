# Despliegue_Puxikka.R

#* Echo back the input

#* @param msg The message to echo
#* @get /echo

function(msg=""){
  list(msg = paste0("The message is: '", msg, "'"))
}

#* Return the shortest path
#* @param JSON the file route to analyze

#* @get /calcPathJSON
function(JSON){
  
  library(tspmeta)
  library(jsonlite)
  
  TSP225 <- fromJSON(JSON)
  coords.df <- data.frame(long=TSP225$Long, lat=TSP225$Lat)
  coords.mx <- as.matrix(coords.df)
  
  # Compute distance matrix
  dist.mx <- dist(coords.mx)
  
  # Construct a TSP object
  tsp.ins <- tsp_instance(coords.mx, dist.mx)
  #
  tour <- run_solver(tsp.ins, method="2-opt")
  #list
  orderedTour <- c()
  for (int in tour){
    orderedTour <- rbind(orderedTour, TSP225[int,])
  }
  orderedTourJSON <- toJSON(orderedTour, pretty = TRUE)
  orderedTour
}
