module utils{

	interface OrganizadorRutas{
		string getRuta();
		void finalizarPedido(int orderID);
	}
}