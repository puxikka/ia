/** @file ClienteIA.java
 *  @brief Class to create the client
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 22/05/2019
 */

/** @brief package controlador
 */
package controlador;


/** @brief Libraries
 */

import service.IAClient;


/**
 * @brief Class ClienteIA
 */
public class ClienteIA{
	
	public ClienteIA() {
		//Empty contructor
	}
	
    public static String llamarGetPathJSON(String json){
        IAClient client = new IAClient();
        return client.getOutput(json);
    }
    
}
