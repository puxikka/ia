/** @file ControladorDB.java
 *  @brief Class to control the access to the database
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 14/05/2019
 */

/** @brief package controlador
 */
package controlador;
/** @brief Libraries
 */

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import modelo.Pedido;
import modelo.Punto;

/**
 * @brief Class ControladorDB
 */
public class ControladorDB {

	private static final Logger LOGGER = Logger.getLogger(ControladorDB.class.getName());
	
	public static final String ORDER_NOT_STARTED = "Sin procesar";
	public static final String ORDER_PROCESSING = "Procesando";
	public static final String ORDER_COMPLETED = "Completado";
	
	private static final String EXCEPTION = "Exception";

	/**
	 * @brief Attributes
	 */
	AdministradorPedidos adminPedidos;

	/**
	 * @brief Constructor
	 */
	public ControladorDB() {
		adminPedidos = new AdministradorPedidos();
		leerPedidosDesdeDB();
	}

	/**
	 * @brief Method for changing status of an order
	 * @param orderID
	 *            The id of the order to change the status
	 * @param newTaskStatus
	 *            The new status of the order. 0=Sin
	 *            procesar/1=procesando/2=completado
	 */
	public static synchronized void cambiarEstadoTaskDB(int orderID, String newOrderStatus) {
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		int statusID;
		if (newOrderStatus.equalsIgnoreCase(ControladorDB.ORDER_NOT_STARTED))
			statusID = 0;
		else if (newOrderStatus.equalsIgnoreCase(ControladorDB.ORDER_PROCESSING))
			statusID = 1;
		else
			statusID = 2;
		try {
			connection = DBManager.getConnection();
			String query = "update pedidos set ESTADO_ID = ? where ID_PEDIDOS = ?"; //
			preparedStmt = connection.prepareStatement(query);
			preparedStmt.setInt(1, statusID);
			preparedStmt.setInt(2, orderID);
			preparedStmt.executeUpdate();

			connection.close();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, EXCEPTION, e);
		} finally {
			closePreparedAndConnection(preparedStmt, connection);
		}
	}

	/**
	 * @brief Method for reading the orders from the database
	 * @return List<Pedido>
	 */
	public static synchronized List<Pedido> leerPedidosDesdeDB() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<Pedido> listaPedidos = Collections.synchronizedList(new ArrayList<>());
		int userId;
		int pedidoId;
		Pedido pedido = null;
		try {
			connection = DBManager.getConnection();
			String selectSql = "SELECT ID_PEDIDOS, pedidos.USUARIO_ID FROM pedidos WHERE estado_id=0;";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectSql);
			while (resultSet.next()) {
				pedidoId = resultSet.getInt(1);
				userId = resultSet.getInt(2);
				pedido = new Pedido(pedidoId, userId);
				cambiarEstadoTaskDB(pedidoId, ORDER_PROCESSING);
				listaPedidos.add(pedido);
				getPointsFromOrder(pedido, userId);
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, EXCEPTION, e);
		} finally {
			closeResultStatementConnection(resultSet, statement, connection);
		}
		return listaPedidos;
	}

	/**
	 * @brief Method for reading the points of an order from the database
	 * @param pedido
	 *            The order to get the points from
	 * @param userId
	 *            The user to deliver the order to
	 */
	private static synchronized void getPointsFromOrder(Pedido pedido, int userId) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int pointID;
		int posX;
		int posY;
		String pointName;
		int pointType;
		Punto punto = null;
		try {
			connection = DBManager.getConnection();
			String selectSql = "SELECT DISTINCT ID_PUNTO, POS_X, POS_Y, NOMBRE_PUNTO, TIPO_PUNTO_ID "
					+ "FROM popbl6.pedidos pedidos "
					+ "JOIN popbl6.rel_product_pedido relPedido ON pedidos.ID_PEDIDOS=relPedido.PEDIDOS_ID "
					+ "JOIN popbl6.product product ON relPedido.PRODUCT_ID=product.ID_PRODUCT "
					+ "JOIN popbl6.punto punto ON product.USUARIO_ID=punto.USER_ID " + "WHERE ID_PEDIDOS="
					+ pedido.getIdPedido() + ";";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectSql);
			while (resultSet.next()) {
				pointID = resultSet.getInt(1);
				posX = resultSet.getInt(2);
				posY = resultSet.getInt(3);
				pointName = resultSet.getString(4);
				pointType = resultSet.getInt(5);
				punto = new Punto(pointID, posX, posY, pointType, pointName);
				pedido.addPuntoToMap(punto, getProductsFromPoint(pedido, punto));
			}
			pedido.addPuntoToMap(getPointFromUser(userId));
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, EXCEPTION, e);
		} finally {
			closeResultStatementConnection(resultSet, statement, connection);
		}
	}

	/**
	 * @brief Method for reading the products of a specific point of an order
	 *        from the database
	 * @param order
	 *            The order to get the points from
	 * @param puntoId
	 *            The point to get the products from
	 * @return Map<String, Integer>
	 */
	private static synchronized Map<String, Integer> getProductsFromPoint(Pedido order, Punto punto) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String prodName;
		int cantidad;
		Map<String, Integer> mapaProductos = new HashMap<>();
		try {
			connection = DBManager.getConnection();
			String selectSql = "SELECT NOMBRE_PRODUCTO, CANTIDAD_PEDIDO FROM popbl6.pedidos pedidos "
					+ "JOIN popbl6.rel_product_pedido relPedido ON pedidos.ID_PEDIDOS=relPedido.PEDIDOS_ID "
					+ "JOIN popbl6.product product ON relPedido.PRODUCT_ID=product.ID_PRODUCT "
					+ "JOIN popbl6.punto punto ON product.USUARIO_ID=punto.USER_ID " + "WHERE PEDIDOS_ID="
					+ order.getIdPedido() + " AND ID_PUNTO=" + punto.getId() + ";";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectSql);
			while (resultSet.next()) {
				prodName = resultSet.getString(1);
				cantidad = resultSet.getInt(2);
				mapaProductos.put(prodName, cantidad);
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, EXCEPTION, e);
		} finally {
			closeResultStatementConnection(resultSet, statement, connection);
		}
		return mapaProductos;
	}

	/**
	 * @brief Method for getting the point of a user
	 * @param userId
	 *            The user to deliver the order to
	 */
	private static synchronized Punto getPointFromUser(int userId) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Punto p = null;
		int pointID;
		int posX;
		int posY;
		String pointName;
		int pointType;

		try {
			connection = DBManager.getConnection();
			String selectSql = "SELECT ID_PUNTO, POS_X,POS_Y,NOMBRE_PUNTO,TIPO_PUNTO_ID FROM popbl6.punto punto "
					+ "WHERE USER_ID=" + userId + ";";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectSql);
			while (resultSet.next()) {
				pointID = resultSet.getInt(1);
				posX = resultSet.getInt(2);
				posY = resultSet.getInt(3);
				pointName = resultSet.getString(4);
				pointType = resultSet.getInt(5);
				p = new Punto(pointID, posX, posY, pointType, pointName);
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, EXCEPTION, e);
		} finally {
			closeResultStatementConnection(resultSet, statement, connection);
		}
		return p;
	}

	/**
	 * @brief Method for getting the value of the adminPedidos variable
	 * @return AdministradorPedidos
	 */
	public AdministradorPedidos getAdminPedidos() {
		return adminPedidos;
	}

	/**
	 * @brief Method for closing the prepared statement and connection
	 * @param preparedStmt
	 * @param connection
	 */
	private static synchronized void closePreparedAndConnection(PreparedStatement preparedStmt, Connection connection) {
		if (preparedStmt != null)
			try {
				preparedStmt.close();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, EXCEPTION, e);
			}
		if (connection != null)
			try {
				connection.close();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, EXCEPTION, e);
			}
	}

	/**
	 * Method for closing the resultSet, statement and connection
	 * 
	 * @param resultSet
	 * @param statement
	 * @param connection
	 */
	private static synchronized void closeResultStatementConnection(ResultSet resultSet, Statement statement,
			Connection connection) {
		if (resultSet != null)
			try {
				resultSet.close();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, EXCEPTION, e);
			}
		if (statement != null)
			try {
				statement.close();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, EXCEPTION, e);
			}
		if (connection != null)
			try {
				connection.close();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, EXCEPTION, e);
			}
	}
}
