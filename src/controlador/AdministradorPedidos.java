/** @file AdministradorPedidos.java
 *  @brief Class to control the orders
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 31/05/2019
 */

/** @brief package controlador
 */
package controlador;
/** @brief Libraries
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import modelo.Pedido;
import modelo.Punto;
import serverRutasICE.ServerRutasICE;

/**
 * @brief Class AdministradorPedidos
 */
public class AdministradorPedidos extends Thread {
	/**
	 * @brief Attributes
	 */
	private static final Logger LOGGER = Logger.getLogger(AdministradorPedidos.class.getName());
	int numHilos;
	ExecutorService executor;
	CompletionService<String> completionService;

	/**
	 * @brief Constructor
	 */
	public AdministradorPedidos() {
		numHilos = Runtime.getRuntime().availableProcessors();
		executor = Executors.newFixedThreadPool(numHilos);
		completionService = new ExecutorCompletionService<>(executor);
	}

	/**
	 * @brief Method to run the thread
	 */
	@Override
	public void run() {
		Future<String> future;
		String rutaString;
		List<Pedido> listaPedidosNuevos;

		while (true) {
			try {
				sleep(5000);
				listaPedidosNuevos = ControladorDB.leerPedidosDesdeDB();
				for (Pedido p : listaPedidosNuevos) {
					completionService.submit(new CalculadorRutaOptima(p));
				}
				for (Pedido p : listaPedidosNuevos) {
					future = completionService.take();
					rutaString = future.get();
					ServerRutasICE.addRutaToList(rutaString);
				}
			} catch (InterruptedException e) {
				this.interrupt();
				LOGGER.log(Level.WARNING, "Thread interrupted", e);
			} catch (ExecutionException e) {
				LOGGER.log(Level.WARNING, "ExecutionException", e);
			}
		}
	}

	public class CalculadorRutaOptima implements Callable<String> {

		Pedido pedido;

		public CalculadorRutaOptima(Pedido pedido) {
			this.pedido = pedido;
		}

		/**
		 * @brief Method for getting the shortest route from an order
		 * @param pedido
		 * @return String
		 */
		@Override
		public String call() throws Exception {
			List<Punto> listaPuntosFinal;
			String orderedPath;
			String json = pedidoToJSON(pedido);
			orderedPath = ClienteIA.llamarGetPathJSON(json);
			listaPuntosFinal = getShortestRoutePointsFromJSON(orderedPath, pedido);
			return pedido.getRutaStringFromOrderedList(listaPuntosFinal);
		}

		/**
		 * @brief Method to convert an order to a JSON string
		 * @param pedido
		 *            The order to be transformed
		 */
		public String pedidoToJSON(Pedido pedido) {
			JsonArray pedidoJson = new JsonArray();
			JsonObject puntoJson = null;
			int numero = 0;
			for (Punto p : pedido.getListaPickupPoint()) {
				numero++;
				puntoJson = new JsonObject();
				puntoJson.addProperty("Id", p.getId());
				puntoJson.addProperty("Long", p.getPosX());
				puntoJson.addProperty("Lat", p.getPosY());
				pedidoJson.add(puntoJson);
			}
			while (numero < 4 && puntoJson != null) {
				numero++;
				pedidoJson.add(puntoJson);
			}
			return pedidoJson.toString();
		}

		/**
		 * @brief Method for getting the list of ordered points from a json
		 *        string
		 * @param pathJSON
		 * @param pedido
		 * @return List<Punto>
		 */
		public List<Punto> getShortestRoutePointsFromJSON(String pathJSON, Pedido pedido) {
			JsonParser jsonParser = new JsonParser();
			JsonArray jsonArray = (JsonArray) jsonParser.parse(pathJSON);
			List<Punto> listaPuntos = new ArrayList<>();

			JsonObject jsonObject;
			JsonElement pIDJSON;
			int pID;
			Punto p;

			for (JsonElement punto : jsonArray) {
				jsonObject = punto.getAsJsonObject();
				pIDJSON = jsonObject.get("Id");
				pID = pIDJSON.getAsInt();
				p = pedido.getPuntoFromID(pID);
				if (!listaPuntos.contains(p))
					listaPuntos.add(p);
			}
			return rotateUntilCorrect(listaPuntos, pedido.getDestinationPoint());
		}

		/**
		 * @brief Method for rotating the point list until the last point is the
		 *        nearest from the destination. It then adds the destination
		 *        point to the list
		 * @param listaPuntos
		 * @param destination
		 * @return List<Punto>
		 */
		private List<Punto> rotateUntilCorrect(List<Punto> listaPuntos, Punto destination) {
			int minDistance = listaPuntos.get(0).manhattanDistanceTo(destination);
			Punto closestPoint = listaPuntos.get(0);
			for (Punto p : listaPuntos) {
				if (p.manhattanDistanceTo(destination) <= minDistance)
					closestPoint = p;
			}
			while (!listaPuntos.get(listaPuntos.size() - 1).equals(closestPoint)) {
				Collections.rotate(listaPuntos, 1);
			}
			listaPuntos.add(destination);
			return listaPuntos;
		}

	}

}
