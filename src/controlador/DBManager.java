/** @file DBManager.java
 *  @brief Class to create the Database Manager
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 07/05/2019
 */

/** @brief package controlador
 */
package controlador;
/** @brief Libraries
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBManager {

	private static final Logger LOGGER = Logger.getLogger(DBManager.class.getName());
	static final String PROPERTIES = "DBConnectionValues";

	static String user;
	static String pass;
	static String dbName;

	public DBManager() {
		//Empty contructor
	}
	
	public static void getValuesFromTextFile(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(PROPERTIES));
			user = prop.getProperty("User","root");
			pass = prop.getProperty("Password","");
			dbName = prop.getProperty("Database","");
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Exception", e);
		}
	}
	
	public static Connection getConnection() {
		Connection connect = null;
		getValuesFromTextFile();
		try {
			// Setup the connection with the DB
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost/" + dbName + "?" + "user=" + user + "&password=" + pass);
			return connect;
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error conectando a la base de datos", e);
		}
		return connect;
	}
}
