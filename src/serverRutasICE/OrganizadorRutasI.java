package serverRutasICE;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.zeroc.Ice.Current;

import controlador.ControladorDB;
import utils.OrganizadorRutas;

public class OrganizadorRutasI implements OrganizadorRutas{
	
	List<String> listaRutas;
	Random r;
	
	public OrganizadorRutasI() {
		this.listaRutas = new ArrayList<>();
		r = new Random();
	}
	
	@Override
	public synchronized String getRuta(Current current) {
		if(listaRutas.isEmpty()) return "No hay rutas disponibles, por favor, intentelo m�s tarde";
		int value = r.nextInt(listaRutas.size());
		String ruta = listaRutas.get(value);
		listaRutas.remove(value);
		return ruta;
	}

	@Override
	public synchronized void finalizarPedido(int orderID, Current current) {
		ControladorDB.cambiarEstadoTaskDB(orderID, ControladorDB.ORDER_COMPLETED);
	}
	
	public synchronized void addRutaToList(String ruta){
		this.listaRutas.add(ruta);
	}

	public synchronized void setListaRutas(List<String> listaRutas) {
		this.listaRutas = listaRutas;
	}
}
