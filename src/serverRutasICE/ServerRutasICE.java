package serverRutasICE;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.zeroc.Ice.Application;
import com.zeroc.Ice.Communicator;
import com.zeroc.Ice.Identity;
import com.zeroc.Ice.ObjectAdapter;

import controlador.AdministradorPedidos;
import controlador.ControladorDB;
import utils.OrganizadorRutas;

public class ServerRutasICE extends Application {
	private static final Logger LOGGER = Logger.getLogger(ServerRutasICE.class.getName());
	static final String PROPERTIES = "RConnectionValues";

	Communicator ic;
	ObjectAdapter adaptador;
	static OrganizadorRutas orgRutas;

	ControladorDB controladorDB;
	AdministradorPedidos adminPedidos;
	ServerRutasICE serverICE;

	@Override
	public int run(String[] arg0) {
		Scanner teclado = new Scanner(System.in);
		iniciarScriptR();

		controladorDB = new ControladorDB();
		adminPedidos = controladorDB.getAdminPedidos();
		adminPedidos.start();

		ic = ServerRutasICE.communicator();
		adaptador = ic.createObjectAdapterWithEndpoints("adaptadorRutas", "tcp -h 127.0.0.1 -p 5000");
		registrarOrgRutas();
		adaptador.activate();
		System.out.println("Servidor de rutas activado. Pulsa RETURN para finalizar...");
		teclado.nextLine();
		teclado.close();
		return 0;
	}

	private void iniciarScriptR() {
		try {
			Process p = Runtime.getRuntime().exec(
					"\""+getRScriptFromTextFile()+"\" \".//IA//Arranque.R\"");
			p.waitFor();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Exception", e);
		}
	}

	public String getRScriptFromTextFile() {
		Properties prop = new Properties();
		String rScript = null;
		try {
			prop.load(new FileInputStream(PROPERTIES));
			rScript = prop.getProperty("RScript", "C:\\Program Files\\R\\R-3.6.0\\bin\\Rscript.exe");
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Exception", e);
		}
		return rScript;
	}

	private void registrarOrgRutas() {
		orgRutas = new OrganizadorRutasI();
		Identity id = new Identity();
		id.name = "OrganizadorRutas";
		adaptador.add(orgRutas, id);
	}

	public static void addRutaToList(String ruta) {
		if (ruta != null) {
			((OrganizadorRutasI) orgRutas).addRutaToList(ruta);
		}
	}

	public static void setListaRutas(List<String> listaRutas) {
		if (listaRutas != null) {
			((OrganizadorRutasI) orgRutas).setListaRutas(listaRutas);
		}
	}

	public static void main(String[] args) {
		ServerRutasICE servidor = new ServerRutasICE();
		int status = servidor.main("Servidor Rutas", args);
		System.exit(status);

	}
}
