/** @file Punto.java
 *  @brief Class to create the points
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 15/05/2019
 */

/** @brief package modelo
 */
package modelo;

/** @brief Libraries
 */


/**
 * @brief Class Punto
 */
public class Punto {
	
	/**
	 * @brief Attributes
	 */
	int id;
	int posX;
	int posY;
	String tipoPunto;
	String nombre;
	
	/**
	 * @brief Constructor
	 * @param id
	 * 			  the id of the point
	 * @param posX
	 *            the position in the x axys
	 * @param posY
	 *            the position in the y axys
	 * @param tipoPunto
	 * 			  the type of point. Pickup or Delivery.
	 * @param nombre
	 * 			  the name of the point
	 */
	public Punto(int id, int posX, int posY, String tipoPunto, String nombre) {
		this.id = id;
		this.posX = posX;
		this.posY = posY;
		this.tipoPunto = tipoPunto;
		this.nombre = nombre;
	}	
	
	/**
	 * @brief Constructor
	 * @param id
	 * 			  the id of the point
	 * @param posX
	 *            the position in the x axys
	 * @param posY
	 *            the position in the y axys
	 * @param tipoPuntoID
	 * 			  the id of the type of point. 1=Pickup or 0=Delivery.
	 * @param nombre
	 * 			  the name of the point
	 */
	public Punto(int id, int posX, int posY, int tipoPuntoID, String nombre) {
		this.id = id;
		this.posX = posX;
		this.posY = posY;
		this.nombre = nombre;
		this.tipoPunto = (tipoPuntoID==1)?"Pickup":"Delivery";
	}
	
	/**
	 * @brief Method for getting the Manhattan distance between 2 points
	 * @param p The point to be calculate the distance with
	 * @return int
	 */
	public int manhattanDistanceTo(Punto p){
		int value;
		int xDiff;
		int yDiff;
		
		xDiff = Math.abs(this.posX-p.getPosX());
		yDiff = Math.abs(this.posY-p.getPosY());
		value = xDiff+yDiff;
		
		return value;
	}

	/**
	 * @brief Method for getting the value of the posX variable
	 * @return int
	 */
	public int getPosX() {
		return posX;
	}
	
	/**
	 * @brief Method for setting the value of the posX variable
	 * @param posX
	 *            new position to set
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	/**
	 * @brief Method for getting the value of the posY variable
	 * @return int
	 */
	public int getPosY() {
		return posY;
	}
	
	/**
	 * @brief Method for setting the value of the posY variable
	 * @param posY
	 *            new position to set
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	/**
	 * @brief Method for getting the value of the tipoPunto variable
	 * @return String
	 */
	public String getTipoPunto() {
		return tipoPunto;
	}
	
	/**
	 * @brief Method for setting the value of the tipoPunto variable
	 * @param tipoPunto
	 *            new type to set
	 */
	public void setTipoPunto(String tipoPunto) {
		this.tipoPunto = tipoPunto;
	}
	
	/**
	 * @brief Method for getting the value of the nombre variable
	 * @return String
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * @brief Method for setting the value of the nombre variable
	 * @param nombre
	 *            new name to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @brief Method for getting the value of the id variable
	 * @return int
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @brief Method for setting the value of the id variable
	 * @param id
	 *            new id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Punto ["+posX +":"+posY+"] -> "+nombre;
	}	
	
}
