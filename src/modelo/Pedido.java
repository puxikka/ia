/** @file Pedido.java
 *  @brief Class to create the orders
 *  @authors
 *  Name          | Surname        | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia         | borja.garciag@alumni.mondragon.edu   |
 *  @date 31/05/2019
 */

/** @brief package modelo
 */
package modelo;
/** @brief Libraries
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @brief Class Pedido
 */
public class Pedido {

	/**
	 * @brief Attributes
	 */
	static final String PICKUP = "Pickup";
	
	int idPedido;
	int idUser;
	Map<Punto, Map<String, Integer>> mapaPuntoProductos;

	/**
	 * @brief Constructor
	 * @param idPedido
	 *            id of the order
	 * @param idUser
	 *            id of the user who made the order
	 */
	public Pedido(int idPedido, int idUser) {
		this.idPedido = idPedido;
		this.idUser = idUser;
		this.mapaPuntoProductos = new HashMap<>();
	}

	/**
	 * @brief Method for getting the list of pickup points
	 * @return List<Punto>
	 */
	public List<Punto> getListaPickupPoint(){
		List<Punto> listaPuntos = new ArrayList<>();
		for(Punto p:this.mapaPuntoProductos.keySet()){
			if(p.getTipoPunto().equals(PICKUP)){
				listaPuntos.add(p);
			}
		}
		return listaPuntos;
	}
	/**
	 * @brief Method for getting the value of the idPedido variable
	 * @return int
	 */
	public int getIdPedido() {
		return idPedido;
	}

	/**
	 * @brief Method for setting the value of the idPedido variable
	 * @param idPedido
	 *            new id to set
	 */
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	/**
	 * @brief Method for getting the value of the idUser variable
	 * @return int
	 */
	public int getIdUser() {
		return idUser;
	}

	/**
	 * @brief Method for setting the value of the idUser variable
	 * @param idUser
	 *            new id to set
	 */
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	/**
	 * @brief Method for getting a point knowing its ID
	 * @param puntoId
	 * @return Punto
	 */
	public Punto getPuntoFromID(int puntoId){
		Punto p = null;
		for(Punto punto:this.mapaPuntoProductos.keySet()){
			if(punto.getId()==puntoId) p=punto;
		}
		return p;
	}
	
	/**
	 * @brief Method for getting the destination point of the order
	 * @return Punto
	 */
	public Punto getDestinationPoint(){
		Punto p = null;
		for(Punto punto:this.mapaPuntoProductos.keySet()){
			if(!punto.getTipoPunto().equals(PICKUP)) p=punto;
		}
		return p;		
	}

	/**
	 * @brief Method for getting the mapaPuntoProductos variable
	 * @return Map<Punto, Map<String, Integer>>
	 */
	public Map<Punto, Map<String, Integer>> getMapaPuntoProductos() {
		return mapaPuntoProductos;
	}

	/**
	 * @brief Method for adding a new point to the mapaPuntoProductos variable
	 * @param punto
	 *            The point to be added
	 */
	public void addPuntoToMap(Punto punto) {
		Map<String, Integer> mapaProductosTotal = new HashMap<>();
		if (punto.getTipoPunto().equals(PICKUP)) {
			this.mapaPuntoProductos.put(punto, null);
		} else {
			for(Punto p:mapaPuntoProductos.keySet()){
				mapaProductosTotal.putAll(mapaPuntoProductos.get(p));
			}
			this.mapaPuntoProductos.put(punto, mapaProductosTotal);
		}
	}

	/**
	 * @brief Method for adding a new point to the mapaPuntoProductos variable
	 * @param punto
	 *            The point to be added
	 */
	public void addPuntoToMap(Punto punto, Map<String, Integer> mapaProductos) {
		this.mapaPuntoProductos.put(punto, mapaProductos);
	}
	
	/**
	 * @brief Method for getting the ordered steps from an order based on an ordered list of points.
	 * @param listaPuntos
	 * @return String
	 */
	public String getRutaStringFromOrderedList(List<Punto> listaPuntos){
		Map<String, Integer> listaProd;
		String accion;
		String s = "Pedido#"+this.idPedido+"->";
		int paso = 1;
		for(Punto p:listaPuntos){
			accion=p.getTipoPunto().equals(PICKUP)?"Recoger":"Entregar";
			s += "\n\t"+paso+".-"+accion+" ";
			paso++;
			listaProd = this.mapaPuntoProductos.get(p);
			if(listaProd!=null){
				for(String producto:listaProd.keySet()){
					s+="'"+producto+"'x"+listaProd.get(producto)+",";
				}
			}
			s+=" en '"+p.getNombre()+"'[" + p.getPosX() + ":" + p.getPosY() + "]";
		}
		return s;
	}
	
	
	@Override
	public String toString() {
		Map<String, Integer> listaProd;
		String accion;
		String s = "Pedido#"+this.idPedido+"->\n";
		for(Punto p:this.mapaPuntoProductos.keySet()){
			accion=p.getTipoPunto().equals(PICKUP)?"Recoger":"Entregar";
			s += "\t-"+accion+" ";
			listaProd = this.mapaPuntoProductos.get(p);
			if(listaProd!=null){
				for(String producto:listaProd.keySet()){
					s+="'"+producto+"'x"+listaProd.get(producto)+",";
				}
			}
			s+=" en "+p.getNombre()+"[" + p.getPosX() + ":" + p.getPosY() + "]";
		}
		return s;
	}

}
