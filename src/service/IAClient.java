package service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class IAClient {

    private static final String BASE_URI = "http://127.0.0.1:8000/calcPathJSON";
	private static final Logger LOGGER = Logger.getLogger(IAClient.class.getName());
    
    private Client client;

    public IAClient() {
        client = Client.create();
    }

	public String getOutput(String json) {
	    WebResource webResource;
	    ClientResponse response;
	    
		String output = null;
    	String encodedJson;
		try {
			encodedJson = URLEncoder.encode(json, java.nio.charset.StandardCharsets.UTF_8.toString());
	        webResource = client
	      		   .resource(BASE_URI+"?JSON="+encodedJson);
	         response = webResource.accept("application/json").get(ClientResponse.class);
	         
	 		if (response.getStatus() != 200) {
	 			   throw new RuntimeException("Failed : HTTP error code : "
	 				+ response.getStatus());
	 			}

	 		output = response.getEntity(String.class);
		} catch (UnsupportedEncodingException e) {
			LOGGER.log(Level.WARNING, "UnsupportedEncodingException", e);
		}
		
		return output;
	}    
    
    
}
